# senselink_viewer

## Environment Variables Setup

Make a copy of .env.example as .env in the project root directory:

```bash
cp .env.example .env
```

Open .env in a text editor, set the following environment variables as needed:

- `SENSELINK_URL`: host address ond port of SenseLink
- `SENSELINK_APP_KEY`: app key of SenseLink
- `SENSELINK_APP_SECRET`: app secret of SenseLink
- `SOCKET_IO_SERVER_URL`: host address and port of the socket io server

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
