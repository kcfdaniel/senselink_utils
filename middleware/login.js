export default function(context) {
    let user = context.$auth.$storage.getUniversal('user')
    if (user) {
        context.$auth.$storage.setUniversal('user', user)
        context.$auth.setUser(user)
    }
}