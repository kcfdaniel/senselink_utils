import CryptoJS from 'crypto-js'
import io from 'socket.io-client';

var socket = null
var healthCheckInitCounter = 0
var batchSendRecordsToHKTInitCounter = 0

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

export const state = () => ({
    devices: [{
        name: "loading...",
        id: "0",
        temperature: 36.0,
        lastUsername: "",
        lastRecordTime: "",
    }],
    students: [],
    currentDeviceId: "0",
    syncStudentProfilesProgress: 0,
})

export const mutations = {
    loadDevices(state, payload) {
        let devices = payload
        state.devices = devices
        state.currentDeviceId = devices[0].id
    },
    loadStudents(state, payload) {
        let students = payload
        state.students = students
    },
    onNewRecord(state, payload) {
        console.log({ payload });
        let record = payload
        let device = state.devices.find(device => device.id === record.sn)
        if ('bodyTemperature' in record) {
            device.temperature = record.bodyTemperature
            device.lastUsername = record.name
            device.lastRecordTime = (new Date(record.signTime * 1000)).toLocaleTimeString()
        }
    },
    mutateState(state, payload) {
        console.log({ payload });
        let [key, value] = Object.entries(payload)[0]
        let keyLayers = key.split(".")
        let currentLayer = state
        for (const keyLayer of keyLayers.slice(0, -1)) {
            currentLayer = currentLayer[keyLayer]
        }
        currentLayer[keyLayers[keyLayers.length - 1]] = value
    },
}

export const actions = {
    async callSenseLinkAPI(context, payload) {
        console.log("callSenseLinkAPI")
        let { senselinkURL, senselinkAppKey, senselinkAppSecret, requestURL, method, params } = payload
        senselinkURL = senselinkURL ? senselinkURL : context.state.auth.user.senselinkURL
        senselinkAppKey = senselinkAppKey ? senselinkAppKey : context.state.auth.user.senselinkAppKey
        senselinkAppSecret = senselinkAppSecret ? senselinkAppSecret : context.state.auth.user.senselinkAppSecret
        console.log({ senselinkURL })
        console.log({ senselinkAppKey })
        console.log({ senselinkAppSecret })
        requestURL = senselinkURL + requestURL
        let app_key = senselinkAppKey
        let app_secret = senselinkAppSecret
        let timestamp = Math.round(+new Date())
        let sign = CryptoJS.MD5(timestamp + '#' + app_secret).toString()

        return await this.$axios({
                method: method || 'get',
                url: requestURL,
                params: {
                    app_key,
                    sign,
                    timestamp,
                    ...params,
                },
            })
            .then((response) => {
                return response.data
            }, (error) => {
                return error
            });
    },
    async loadDevices(context, payload) {
        let response = await context.dispatch("callSenseLinkAPI", {
            ...payload,
            requestURL: "/api/v3/device"
        })
        console.log({ devices: response.data.device_list })
        let devices = response.data.device_list.map(device => {
            const existingDevice = context.state.devices.find(existingDevice => existingDevice.id === device.ldid)
            return {
                ...device,
                id: device.ldid,
                temperature: existingDevice ? existingDevice.temperature : 36,
                lastUsername: existingDevice ? existingDevice.lastUsername : "",
                lastRecordTime: existingDevice ? existingDevice.lastRecordTime : "",
            }
        })
        context.commit('loadDevices', devices)
    },
    async loadStudents(context, payload) {
        const response = await context.dispatch("callSenseLinkAPI", {
            ...payload,
            requestURL: "/api/v1/user/list"
        })
        console.log({ students: response.data })
        const students = response.data
        context.commit('loadStudents', students)
        return response
    },
    async updateStudentProfiles(context, payload) {
        let responses = []
        let response = null

        const studentProfiles = [{
                id: "29317263", //daniel
                icNumber: "2345678987654"
            },
            {
                id: "15528030", //jack
                icNumber: "345676543"
            },
            {
                id: "29317263", //daniel
                icNumber: "2345678987654"
            },
            {
                id: "15528030", //jack
                icNumber: "345676543"
            },
            {
                id: "29317263", //daniel
                icNumber: "2345678987654"
            },
            {
                id: "15528030", //jack
                icNumber: "345676543"
            },
            {
                id: "29317263", //daniel
                icNumber: "2345678987654"
            },
            {
                id: "15528030", //jack
                icNumber: "345676543"
            },
            {
                id: "29317263", //daniel
                icNumber: "2345678987654"
            },
            {
                id: "15528030", //jack
                icNumber: "345676543"
            },
            {
                id: "29317263", //daniel
                icNumber: "2345678987654"
            },
            {
                id: "15528030", //jack
                icNumber: "345676543"
            },
        ]

        for (const [index, studentProfile] of studentProfiles.entries()) {
            response = await context.dispatch("callSenseLinkAPI", {
                ...payload,
                method: 'post',
                requestURL: "/api/v2/user/update/" + studentProfile.id,
                params: {
                    ...studentProfile,
                },
            })
            responses.push(response)
            context.commit('mutateState', {
                syncStudentProfilesProgress: (index / studentProfiles.length) * 100
            })
        }

        return responses
    },
    connectSocket(context, payload) {
        let { socketIOURL, socketIOPath, successCallback, failureCallback } = payload
        socketIOURL = socketIOURL ? socketIOURL : context.state.auth.user.socketIOURL
        socketIOPath = socketIOPath ? socketIOPath : context.state.auth.user.socketIOPath

        socket = io(socketIOURL, {
            timeout: 2000,
            path: socketIOPath,
        });

        socket.on('connect', () => {
            console.log("socket io connected!")
            if (successCallback) {
                successCallback()
            }
        })

        socket.on('connect_error', (error) => {
            console.log("socket io connect error:", error)
            socket.disconnect()
            if (failureCallback) {
                failureCallback(socket)
            }
        })

        socket.on('error', (error) => {
            console.log("socket io error:", error)
            socket.disconnect()
            if (failureCallback) {
                failureCallback(socket)
            }
        });

        socket.on('connect_timeout', (error) => {
            console.log("socket io connect timeout:", error)
            socket.disconnect()
            if (failureCallback) {
                failureCallback(socket)
            }
        })

        socket.on('on_new_record', (data) => {
            console.log("received on_new_record event:", data)
            context.commit('onNewRecord', data)
        })
    },
    disconnectSocket(context, payload) {
        if (socket) {
            socket.disconnect()
        } else {
            console.log("socket not initialized yet, so not disconnected")
        }
    },
    startDaemonWork(context, payload) {
        console.log("startDaemonWork")
        context.dispatch("startHealthCheck")
        context.dispatch("startBatchSendRecordsToHKT")
    },
    async startHealthCheck(context, payload) {
        healthCheckInitCounter++
        let id = healthCheckInitCounter
        while (id == healthCheckInitCounter) {
            console.log("healthCheck")
            context.dispatch('loadDevices')
            await sleep(context.state.auth.user.healthCheckInterval * 1000)
        }
    },
    async startBatchSendRecordsToHKT(context, payload) {
        batchSendRecordsToHKTInitCounter++
        let id = batchSendRecordsToHKTInitCounter
        while (id == batchSendRecordsToHKTInitCounter) {
            console.log("batchSendRecordsToHKT")
            await sleep(context.state.auth.user.batchSendRecordsToHKTInterval * 1000)
        }
    },
}