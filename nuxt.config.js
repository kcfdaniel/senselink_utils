import colors from 'vuetify/es5/util/colors'

let senselinkURL = process.env.SENSELINK_URL || 'https://link.bi.sensetime.com'

export default {
    vuetify: {
        customVariables: ['~/assets/variables.scss'],
        theme: {
            // dark: true,
            themes: {
                light: {
                    tertiary: colors.grey.lighten4,
                },
                dark: {
                    primary: colors.blue.darken2,
                    accent: colors.grey.darken3,
                    secondary: colors.amber.darken3,
                    info: colors.teal.lighten1,
                    warning: colors.amber.base,
                    error: colors.deepOrange.accent4,
                    success: colors.green.accent3,
                    tertiary: colors.black,
                }
            }
        }
    },
    serverMiddleware: [
        // API middleware
        '~/api/index.js'
    ],
    router: {
        middleware: ['login', 'auth']
    },
    publicRuntimeConfig: {
        senselinkURL,
        senselinkAppKey: process.env.SENSELINK_APP_KEY,
        senselinkAppSecret: process.env.SENSELINK_APP_SECRET,
    },
    privateRuntimeConfig: {

    },
    /*
     ** Nuxt rendering mode
     ** See https://nuxtjs.org/api/configuration-mode
     */
    mode: 'universal',
    /*
     ** Nuxt target
     ** See https://nuxtjs.org/api/configuration-target
     */
    target: 'server',
    /*
     ** Headers of the page
     ** See https://nuxtjs.org/api/configuration-head
     */
    head: {
        titleTemplate: '%s - ' + process.env.npm_package_name,
        title: process.env.npm_package_name || '',
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
        ],
        link: [
            { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
        ]
    },
    /*
     ** Global CSS
     */
    css: [],
    /*
     ** Plugins to load before mounting the App
     ** https://nuxtjs.org/guide/plugins
     */
    plugins: ['@/plugins/vuejs-thermometer.js'],
    /*
     ** Auto import components
     ** See https://nuxtjs.org/api/configuration-components
     */
    components: true,
    /*
     ** Nuxt.js dev-modules
     */
    buildModules: [
        '@nuxtjs/vuetify',
    ],
    /*
     ** Nuxt.js modules
     */
    modules: [
        // Doc: https://axios.nuxtjs.org/usage
        '@nuxtjs/axios',
        '@nuxtjs/auth',
    ],
    proxy: {
        '/senselink/api': {
            target: senselinkURL,
            pathRewrite: {
                '^/senselink': '/'
            }
        }
    },
    /*
     ** Axios module configuration
     ** See https://axios.nuxtjs.org/options
     */
    axios: {
        proxy: true,
    },
    /*
     ** vuetify module configuration
     ** https://github.com/nuxt-community/vuetify-module
     */
    /*
     ** Build configuration
     ** See https://nuxtjs.org/api/configuration-build/
     */
    build: {}
}