import Vue from 'vue'
if (process.browser) {
    let VueThermometer = require('vuejs-thermometer')
    Vue.use(VueThermometer)
}