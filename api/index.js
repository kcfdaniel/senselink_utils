//sample code only, not used
const express = require('express')
const bodyParser = require('body-parser')

// Create app
const app = express()

// Install middleware
app.use(bodyParser.json())

// [GET] /
app.get('/', function(req, res) {
    console.log({ body: req.body })
    console.log({ user: req.user })
    res.send({
        message: 'OK'
    })
})

// Export the server middleware
module.exports = {
    path: '/api',
    handler: app
}